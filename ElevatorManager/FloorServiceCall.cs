﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager
{
    /// <summary>
    /// Represents a floor service call made by passenger from floor to either move up or down
    /// </summary>
    internal class FloorServiceCall
    {
        public FloorServiceCall(int floor, ElevatorMovementDirection direction)
        {
            Floor = floor;
            Direction = direction;
        }

        public int Floor { get; }
        public ElevatorMovementDirection Direction { get; }
    }

    /// <summary>
    /// Allows comparing 2 instances of FloorServiceCalls which is used for sorting
    /// </summary>
    internal class FloorServiceCallComparer : IComparer<FloorServiceCall>, IEqualityComparer<FloorServiceCall>
    {
        public int Compare(FloorServiceCall x, FloorServiceCall y)
        {
            if (x.Floor == y.Floor)
            {
                if (x.Direction == y.Direction)
                    return 0;

                if ((int)x.Direction < (int)y.Direction)
                    return -1;
                else
                    return 1;
            }
            else
            {
                if (x.Floor < y.Floor)
                    return -1;
                else
                    return 1;
            }
        }

        public bool Equals(FloorServiceCall x, FloorServiceCall y)
        {
            return Compare(x, y) == 0;
        }

        public int GetHashCode(FloorServiceCall obj)
        {
            return obj.Floor;
        }
    }
}
