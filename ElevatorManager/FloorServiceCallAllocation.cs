﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager
{
    /// <summary>
    /// FloorServiceCallAllocation maintains the allocation of elevator against requests made
    /// </summary>
    internal class FloorServiceCallAllocation : IDisposable
    {
        private readonly Dictionary<FloorServiceCall, string> floorServiceAllocation;
        readonly List<Elevator> elevators;
        private readonly int minimumFloor;
        private readonly int maximumFloor;
        private ReaderWriterLockSlim stateLock;
        private bool isDisposed;

        public FloorServiceCallAllocation(List<Elevator> elevators, int minimumFloor, int maximumFloor)
        {
            isDisposed = false;
            this.elevators = elevators;
            this.minimumFloor = minimumFloor;
            this.maximumFloor = maximumFloor;
            floorServiceAllocation = new Dictionary<FloorServiceCall, string>(new FloorServiceCallComparer());
            stateLock = new ReaderWriterLockSlim();
        }

        /// <summary>
        /// Allocates an elevator for a incoming request
        /// </summary>
        /// <param name="floorServiceCall"></param>
        /// <returns></returns>
        internal bool AllocateElevator(FloorServiceCall floorServiceCall)
        {
            stateLock.EnterWriteLock();
            try
            {
                if (floorServiceAllocation.ContainsKey(floorServiceCall))
                    return false;

                Elevator matchingElevator;
                var matchingElevators = elevators.Where(elevator => IsElevatorInRightDirection(elevator, floorServiceCall)).Select((elevator) => GetElevatorDistance(elevator, floorServiceCall.Floor));
                if (matchingElevators.Any())
                    matchingElevator = matchingElevators.OrderBy(x => x.Item2).First().Item1;
                else
                    matchingElevator = elevators.Select((elevator) => GetElevatorDistance(elevator, floorServiceCall.Floor)).OrderBy(x => x.Item2).First().Item1;
                AllocateElevator(floorServiceCall, matchingElevator);
                return true;
            }
            finally
            {
                stateLock.ExitWriteLock();
            }
        }

        private void AllocateElevator(FloorServiceCall floorServiceCall, Elevator elevator)
        {
            floorServiceAllocation.Add(floorServiceCall, elevator.Name);
            Task.Run(() =>
            {
                elevator.ServeFloor(floorServiceCall.Floor, floorServiceCall.Direction);
            });
        }

        /// <summary>
        /// Deallocates elevator when user undo request
        /// </summary>
        /// <param name="floorServiceCall"></param>
        /// <returns></returns>
        internal bool DeallocateElevator(FloorServiceCall floorServiceCall)
        {
            stateLock.EnterWriteLock();
            try
            {
                if (!floorServiceAllocation.ContainsKey(floorServiceCall))
                    return false;

                Elevator matchingElevator = elevators.First(x => x.Name == floorServiceAllocation[floorServiceCall]);
                Task.Run(() =>
                {
                    matchingElevator.CancelFloorService(floorServiceCall.Floor, floorServiceCall.Direction);
                });
                return true;
            }
            finally
            {
                stateLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// For a given elevator, revists the allocation so that specified elevator can be allocated to any request considering its latest floor and direction
        /// </summary>
        /// <param name="elevator"></param>
        /// <returns></returns>
        internal bool RevisitAllocation(Elevator elevator)
        {
            stateLock.EnterWriteLock();
            try
            {
                var status = elevator.State.GetStatus();
                var floorServiceCall = GetFloorServiceCallForAllocation(status);
                if (!floorServiceAllocation.ContainsKey(floorServiceCall))
                {
                    AllocateElevator(floorServiceCall, elevator);
                    return true;
                }
                else
                {
                    Elevator matchingElevator = elevators.First(x => x.Name == floorServiceAllocation[floorServiceCall]);
                    var matchingElevatorStatus = matchingElevator.State.GetStatus();
                    var matchingElevatorFloorServiceCall = GetFloorServiceCallForAllocation(matchingElevatorStatus);
                    if (floorServiceCall.Direction == matchingElevatorFloorServiceCall.Direction && floorServiceCall.Floor == matchingElevatorFloorServiceCall.Floor)
                        return false;
                    Task.Run(() =>
                    {                    
                        matchingElevator.CancelFloorService(floorServiceCall.Floor, floorServiceCall.Direction);
                    });
                    AllocateElevator(floorServiceCall, elevator);
                    return true;
                }
            }
            finally
            {
                stateLock.ExitWriteLock();
            }
        }

        private FloorServiceCall GetFloorServiceCallForAllocation(ElevatorStatus status)
        {
            switch (status.Direction)
            {
                case ElevatorMovementDirection.Up:
                    if ((status.CurrentFloor == maximumFloor))
                        return new FloorServiceCall(status.CurrentFloor - 1, ElevatorMovementDirection.Down);
                    return new FloorServiceCall(status.CurrentFloor + 1, ElevatorMovementDirection.Up);
                default:
                    if ((status.CurrentFloor == minimumFloor))
                        return new FloorServiceCall(status.CurrentFloor + 1, ElevatorMovementDirection.Up);
                    return new FloorServiceCall(status.CurrentFloor - 1, ElevatorMovementDirection.Down);

            }
        }

        private bool IsElevatorInRightDirection(Elevator elevator, FloorServiceCall floorServiceCall)
        {
            var status = elevator.State.GetStatus();
            return (status.Direction == ElevatorMovementDirection.None || status.Direction == floorServiceCall.Direction);
        }

        private Tuple<Elevator, int> GetElevatorDistance(Elevator elevator, int targetFloor)
        {
            var status = elevator.State.GetStatus();
            return Tuple.Create(elevator, Math.Abs(status.CurrentFloor - targetFloor));
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!isDisposed)
            {
                stateLock.Dispose();
                isDisposed = true;
            }
        }


        ~FloorServiceCallAllocation()
        {
            Dispose(false);
        }
    }


}
