﻿using Eurofins.ElevatorManager.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager
{
    /// <summary>
    /// FloorRequestHandler tracks all floor request from each floor as well as target floor request from within elevator.
    /// It is used to assist elevator to  decide which flor or direction to move next
    /// </summary>
    internal class FloorRequestHandler : IDisposable
    {

        public const int NO_FLOOR_TO_MOVE = int.MinValue;
        public const string INVALID_MIN_FLOOR_CONFIG = "Min floor value '{0}' is invalid floor configuration value";
        public const string INVALID_MAX_FLOOR_CONFIG = "Max floor configuration value '{0} is less than Min floor configuration value '{1}'";
        public const string INVALID_ELEVATOR_DIRECTION = "Elevator can be requested for either upward or downward movement only";

        private bool isDisposed;

        /// <summary>
        /// Maintains target floor numbers requested by passengers from inside elevator
        /// </summary>
        private readonly SortedSet<int> passengerFloorRequests;

        /// <summary>
        /// Maintains floor numbers from where passengers called for the lift
        /// </summary>
        private readonly SortedSet<FloorServiceCall> passengerFloorServiceCalls;

        /// <summary>
        /// Read/Write object to safely access collections 
        /// </summary>
        private readonly ReaderWriterLockSlim readWriteLock;

        /// <summary>
        /// minimum floor that elevator can serve
        /// </summary>
        private readonly int minFloor;

        /// <summary>
        /// maximum floor that elevator can serve
        /// </summary>
        private readonly int maxFloor;

        /// <summary>
        /// Creates an instance of Floor request handler initializing its internal states
        /// </summary>
        /// <param name="minFloor"></param>
        /// <param name="maxFloor"></param>
        public FloorRequestHandler(int minFloor, int maxFloor)
        {
            if (minFloor == NO_FLOOR_TO_MOVE)
                throw new InvalidFloorException(string.Format(INVALID_MIN_FLOOR_CONFIG, minFloor));
            if (maxFloor < minFloor)
                throw new InvalidFloorException(string.Format(INVALID_MAX_FLOOR_CONFIG, maxFloor, minFloor));

            isDisposed = false;
            this.minFloor = minFloor;
            this.maxFloor = maxFloor;
            passengerFloorRequests = new SortedSet<int>();
            passengerFloorServiceCalls = new SortedSet<FloorServiceCall>(new FloorServiceCallComparer());
            readWriteLock = new ReaderWriterLockSlim();
        }

        /// <summary>
        /// Checks whether there are any requests made by passengers either from floor or from inside elevator
        /// </summary>
        /// <returns></returns>
        public bool HasTargetFloorsToMove()
        {
            readWriteLock.EnterReadLock();
            var result = !(passengerFloorServiceCalls.Count == 0 && passengerFloorRequests.Count == 0);
            readWriteLock.ExitReadLock();
            return result;
        }

        /// <summary>
        /// Based on the direction, registers floor request made by passenger from floor
        /// </summary>
        /// <param name="floor"></param>
        /// <param name="directionToServe"></param>
        public bool RegisterFloorServiceCall(int floor, ElevatorMovementDirection directionToServe)
        {
            if (directionToServe == ElevatorMovementDirection.None)
                throw new InvalidElevatorDirectionException(INVALID_ELEVATOR_DIRECTION);

            readWriteLock.EnterWriteLock();
            try
            {
                if (passengerFloorServiceCalls.FirstOrDefault(x => x.Floor == floor && x.Direction == directionToServe) == null)
                {
                    passengerFloorServiceCalls.Add(new FloorServiceCall(floor, directionToServe));
                    return true;
                }
            }
            finally
            {
                readWriteLock.ExitWriteLock();
            }
            return false;
        }

        /// <summary>
        /// Based on the direction, unregisters floor request made by passenger from floor
        /// </summary>
        /// <param name="floor"></param>
        /// <param name="directionToServe"></param>
        public bool UnregisterFloorServiceCall(int floor, ElevatorMovementDirection directionToServe)
        {
            if (directionToServe == ElevatorMovementDirection.None)
                throw new InvalidElevatorDirectionException(INVALID_ELEVATOR_DIRECTION);

            readWriteLock.EnterWriteLock();
            try
            {
                var floorServiceCall = passengerFloorServiceCalls.FirstOrDefault(x => x.Floor == floor && x.Direction == directionToServe);
                if (floorServiceCall != null)
                {
                    passengerFloorServiceCalls.Remove(floorServiceCall);
                    return true;
                }
            }
            finally
            {
                readWriteLock.ExitWriteLock();
            }
            return false;
        }


        /// <summary>
        /// Registers floor request made by passenger inside elevator
        /// </summary>
        /// <param name="floor"></param>
        public void RegisterPassengerFloorRequest(int floor)
        {
            readWriteLock.EnterWriteLock();
            try
            {
                if (!passengerFloorRequests.Contains(floor))
                {
                    passengerFloorRequests.Add(floor);
                }
            }
            finally
            {
                readWriteLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Unregisters floor request made by passenger inside elevator
        /// </summary>
        /// <param name="floor"></param>
        public void UnregisterPassengerFloorRequest(int floor)
        {
            readWriteLock.EnterWriteLock();
            try
            {
                if (passengerFloorRequests.Contains(floor))
                {
                    passengerFloorRequests.Remove(floor);
                }
            }
            finally
            {
                readWriteLock.ExitWriteLock();
            }
        }

        /// <summary>
        /// Identifies the next possible floor the elevator has to move based on the requests made so far in the direction that elevator is moving
        /// </summary>
        /// <param name="direction"></param>
        /// <param name="currentFloor"></param>
        /// <returns></returns>
        public int GetNextFloorRequested(ElevatorMovementDirection direction, int currentFloor)
        {
            if (currentFloor < minFloor || currentFloor > maxFloor)
                return NO_FLOOR_TO_MOVE;

            int nextFloorToMove;
            readWriteLock.EnterReadLock();
            try
            {
                var passengerCallFloors = passengerFloorServiceCalls.Select(x => x.Floor).Distinct().Cast<int?>();
                var nextFloorCall = GetNextFloorToMove(direction, currentFloor, passengerCallFloors);
                var nextFloorRequest = GetNextFloorToMove(direction, currentFloor, passengerFloorRequests.Cast<int?>());
                nextFloorToMove = GetNextFloorToMove(direction, currentFloor, nextFloorCall, nextFloorRequest);
            }
            finally
            {
                readWriteLock.ExitReadLock();
            }
            return nextFloorToMove;
        }

        private int GetNextFloorToMove(ElevatorMovementDirection direction, int currentFloor, IEnumerable<int?> floorRequests)
        {
            var greaterValue = floorRequests.FirstOrDefault(floor => floor > currentFloor);
            var smallerValue = floorRequests.LastOrDefault(floor => floor < currentFloor);

            var higherFloor = greaterValue.HasValue ? greaterValue.Value : NO_FLOOR_TO_MOVE;
            var lowerFloor = smallerValue.HasValue ? smallerValue.Value : NO_FLOOR_TO_MOVE;

            if (higherFloor == lowerFloor && lowerFloor == NO_FLOOR_TO_MOVE)
                return NO_FLOOR_TO_MOVE;

            switch (direction)
            {
                case ElevatorMovementDirection.Up:
                    return (higherFloor != NO_FLOOR_TO_MOVE) ? higherFloor : lowerFloor;

                case ElevatorMovementDirection.Down:
                    return (lowerFloor != NO_FLOOR_TO_MOVE) ? lowerFloor : higherFloor;

                default:
                    if (higherFloor != NO_FLOOR_TO_MOVE && lowerFloor != NO_FLOOR_TO_MOVE)
                        return Math.Abs(higherFloor - currentFloor) < Math.Abs(currentFloor - lowerFloor) ? higherFloor : lowerFloor;
                    if (lowerFloor != NO_FLOOR_TO_MOVE)
                        return lowerFloor;
                    else
                        return higherFloor;
            }
        }

        private static int GetNextFloorToMove(ElevatorMovementDirection direction, int currentFloor, int nextFloorCall, int nextFloorRequest)
        {
            if (nextFloorCall == nextFloorRequest && nextFloorCall == NO_FLOOR_TO_MOVE)
                return NO_FLOOR_TO_MOVE;

            if (nextFloorCall == NO_FLOOR_TO_MOVE)
                return nextFloorRequest;

            if (nextFloorRequest == NO_FLOOR_TO_MOVE)
                return nextFloorCall;


            if (currentFloor < nextFloorCall && currentFloor < nextFloorRequest)
                return (nextFloorCall < nextFloorRequest) ? nextFloorCall : nextFloorRequest;
            if (currentFloor > nextFloorCall && currentFloor > nextFloorRequest)
                return (nextFloorCall > nextFloorRequest) ? nextFloorCall : nextFloorRequest;

            switch (direction)
            {
                case ElevatorMovementDirection.Up:
                    if (currentFloor < nextFloorCall && currentFloor > nextFloorRequest)
                        return nextFloorCall;
                    else
                        return nextFloorRequest;
                case ElevatorMovementDirection.Down:
                    if (currentFloor > nextFloorCall && currentFloor < nextFloorRequest)
                        return nextFloorCall;
                    else
                        return nextFloorRequest;
                default:
                    return GetNextFloorToMove(currentFloor, nextFloorCall, nextFloorRequest);

            }
        }

        private static int GetNextFloorToMove(int currentFloor, int nextFloorCall, int nextFloorRequest)
        {            
            var distanceFromCalledFloor = Math.Abs(currentFloor - nextFloorCall);
            var distanceFromRequestedFloor = Math.Abs(currentFloor - nextFloorRequest);
            return (distanceFromCalledFloor < distanceFromRequestedFloor) ? nextFloorCall : nextFloorRequest;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);

        }

        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                readWriteLock.Dispose();
                if (disposing)
                {
                    passengerFloorServiceCalls.Clear();
                    passengerFloorRequests.Clear();
                }
                isDisposed = true;
            }
        }

        ~FloorRequestHandler()
        {
            Dispose(false);
        }
    }
}
