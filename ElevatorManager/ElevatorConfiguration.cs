﻿using Eurofins.ElevatorManager.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager
{
    /// <summary>
    /// Elevator operational configurations are maintained in this class
    /// </summary>
    public class ElevatorConfiguration
    {

        public const string INVALID_DURATION = "Duration cannot be negative";

        /// <summary>
        /// Creates a configuratyion instance with specified floor values and all durations set to 1 second
        /// </summary>
        /// <param name="minimumFloor">minimum floor that elevator can serve</param>
        /// <param name="maximumFloor">maximum floor that elevator can serve and must be greater than or equal to minimumFloor</param>
        public ElevatorConfiguration(int minimumFloor, int maximumFloor)
        {
            GuardFloorNumbers(minimumFloor, maximumFloor);
            
            MinimumFloor = maximumFloor;
            MaximumFloor = maximumFloor;
            FloorMovementDuration = DoorMovementDuration = PassengerMovementDuration = 1000;
        }

        public ElevatorConfiguration(int floorMovementDuration, int doorMovementDuration, int passengerMovementDuration, int minimumFloor, int maximumFloor)
        {
            if (floorMovementDuration < 0)
                throw new ArgumentException(INVALID_DURATION, nameof(floorMovementDuration));
            if (doorMovementDuration < 0)
                throw new ArgumentException(INVALID_DURATION, nameof(doorMovementDuration));
            if (passengerMovementDuration < 0)
                throw new ArgumentException(INVALID_DURATION, nameof(passengerMovementDuration));
            GuardFloorNumbers(minimumFloor, maximumFloor);
            
            FloorMovementDuration = floorMovementDuration;
            DoorMovementDuration = doorMovementDuration;
            PassengerMovementDuration = passengerMovementDuration;
            MinimumFloor = minimumFloor;
            MaximumFloor = maximumFloor;
        }

        private void GuardFloorNumbers(int minimumFloor, int maximumFloor)
        {
            if (minimumFloor == FloorRequestHandler.NO_FLOOR_TO_MOVE)
                throw new InvalidFloorException(string.Format(FloorRequestHandler.INVALID_MIN_FLOOR_CONFIG, minimumFloor));
            if (maximumFloor < minimumFloor)
                throw new InvalidFloorException(string.Format(FloorRequestHandler.INVALID_MAX_FLOOR_CONFIG, maximumFloor, minimumFloor));
        }

        /// <summary>
        /// Time taken in milliseconds to move between floors
        /// </summary>
        public int FloorMovementDuration { get; private set; }

        /// <summary>
        /// Time taken in milliseconds to either fully close/open the elevator
        /// </summary>
        public int DoorMovementDuration { get; private set; }

        /// <summary>
        /// Time taken in milliseconds for lift to wait for passengers to move in/out after opening the lift door
        /// </summary>
        public int PassengerMovementDuration { get; private set; }

        /// <summary>
        /// Lowest floor number that the elevator has to serve
        /// </summary>
        public int MinimumFloor { get; private set; }

        /// <summary>
        /// Highest floor number that the elevator has to serve
        /// </summary>
        public int MaximumFloor { get; private set; }

    }
}
