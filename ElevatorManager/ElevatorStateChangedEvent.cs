﻿using Eurofins.Infrastructure;
using Mediator.Net.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Unity;

namespace Eurofins.ElevatorManager
{
    /// <summary>
    /// Event representing state transition change
    /// </summary>
    public class ElevatorStateChangedEvent : IEvent
    {        

        public ElevatorStateChangedEvent(ElevatorStatus elevatorStatus)
        {
            this.ElevatorStatus = elevatorStatus;
            var unity = Infrastructure.Environment.Instance.UnityContainer;
            this.EventId = unity.Resolve<ISequenceGenerator>().GetNext();
            this.ThreadId = Thread.CurrentThread.ManagedThreadId;
        }

        public ElevatorStatus ElevatorStatus { get; }

        public int EventId { get; }

        public int ThreadId { get; }
    }
}
