﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager
{
    /// <summary>
    /// ElevatorBuilder accepts configurations and elevators available and generates a ElevatorSynchronizer which will start accepting user inputs
    /// </summary>
    public class ElevatorBuilder
    {
        private int floorMovementDuration;

        private int doorMovementDuration;

        private int passengerMovementDuration;

        private int minimumFloor;

        private int maximumFloor;

        private readonly List<string> elevatorNames;

        public ElevatorBuilder()
        {
            elevatorNames = new List<string>();
        }

        public ElevatorSynchronizer Build()
        {
            if (elevatorNames.Count == 0)
                throw new InvalidOperationException("Atleast one elevator should be added");

            var config = new ElevatorConfiguration(floorMovementDuration, doorMovementDuration, passengerMovementDuration, minimumFloor, maximumFloor);
            
            return new ElevatorSynchronizer(elevatorNames, config);
        }

        public ElevatorBuilder SetElevatorFloorMovementDuration(int floorMovementDuration)
        {
            this.floorMovementDuration = floorMovementDuration;
            return this;
        }

        public ElevatorBuilder SetElevatorDoorMovementDuration(int doorMovementDuration)
        {
            this.doorMovementDuration = doorMovementDuration;
            return this;
        }

        public ElevatorBuilder SetPassengerWaitTime(int passengerMovementDuration)
        {
            this.passengerMovementDuration = passengerMovementDuration;
            return this;
        }

        public ElevatorBuilder SetFloorServiceRange(int minimumFloor, int maximumFloor)
        {
            this.minimumFloor = minimumFloor;
            this.maximumFloor = maximumFloor;
            return this;
        }

        public ElevatorBuilder AddElevator(string name)
        {
            if (elevatorNames.Contains(name))
                throw new ArgumentException("Elevator with same name already exists", nameof(name));

            elevatorNames.Add(name);
            return this;
        }

    }
}
