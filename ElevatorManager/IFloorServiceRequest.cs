﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager
{
    public interface IFloorServiceRequest
    {
        bool ServeFloor(int floor, ElevatorMovementDirection direction);

        bool CancelFloorService(int floor, ElevatorMovementDirection direction);
    }
}
