﻿using Eurofins.ElevatorManager.ElevatorStates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager
{
    /// <summary>
    /// ElevatorStatus is an immutable class that represents key states of elevator. 
    /// This is used to take snapshot of elevator state as ever running background thread keep them changing
    /// </summary>
    public struct ElevatorStatus
    {
        public ElevatorStatus(string name, ElevatorStateEnum state, int currentFloor, ElevatorMovementDirection direction)
        {
            ElevatorName = name;
            State = state;
            CurrentFloor = currentFloor;
            Direction = direction;
            switch(state)
            {
                case ElevatorStateEnum.DoorOpened:
                case ElevatorStateEnum.WaitingForPassenger:
                    DoorStage = ElevatorDoorState.MIN_DOOR_STAGE;
                    break;
                default:
                    DoorStage = ElevatorDoorState.MAX_DOOR_STAGE;
                    break;
            }
        }

        public ElevatorStatus(string name, ElevatorStateEnum state, int currentFloor, ElevatorMovementDirection direction, int doorStage)
        {
            ElevatorName = name;
            State = state;
            CurrentFloor = currentFloor;
            Direction = direction;
            DoorStage = doorStage;
        }

        public string ElevatorName { get; }
        public ElevatorStateEnum State { get; }
        public int CurrentFloor { get; }
        public ElevatorMovementDirection Direction { get; }
        public int DoorStage { get; }

    }
}
