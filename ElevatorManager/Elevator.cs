﻿using Eurofins.ElevatorManager.ElevatorStates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Eurofins.ElevatorManager
{
    /// <summary>
    /// Represents an Elevator
    /// </summary>
    public class Elevator : IFloorServiceRequest, IDisposable
    {

        private const string INVALID_NAME = "Elevator Name cannot be null or empty";
        private const string INVALID_CONFIGURATION = "Elevator configuration cannot be null";

        private bool isDisposed;
        private ElevatorState state;
        private readonly StateTransitionManager stateTransitionManager;

        /// <summary>
        /// Operational configuration of the elevator
        /// </summary>
        public ElevatorConfiguration Configuration { get; }


        public Elevator(string name, ElevatorConfiguration configuration)
        {
            if (string.IsNullOrWhiteSpace(name))
                throw new ArgumentNullException(nameof(name), INVALID_NAME);
            if (configuration == null)
                throw new ArgumentException(INVALID_CONFIGURATION, nameof(configuration));

            Name = name;
            Configuration = configuration;
            stateTransitionManager = new StateTransitionManager(this);
            state = new ElevatorHalted(this, stateTransitionManager);
            isDisposed = false;
        }

        internal ElevatorState State
        {
            get
            {
                return state;
            }
            set
            {
                state = value;
                RaiseStateChanged();
            }
        }        

        private void RaiseStateChanged()
        {
            var status = state.GetStatus();            
            Infrastructure.Environment.Instance.Mediator.PublishAsync(new ElevatorStateChangedEvent(status));
        }

        public string Name { get; private set; }
                

        private bool CanServeFloor(int floor)
        {
            return (floor >= Configuration.MinimumFloor && floor <= Configuration.MaximumFloor);
        }

        /// <summary>
        /// Addresses service call made from floor
        /// </summary>
        /// <param name="floor"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public bool ServeFloor(int floor, ElevatorMovementDirection direction)
        {
            if (CanServeFloor(floor))
            {
                state.RegisterFloorServiceCall(floor, direction);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Addresses cancellation of service call made from floor
        /// </summary>
        /// <param name="floor"></param>
        /// <param name="direction"></param>
        /// <returns></returns>
        public bool CancelFloorService(int floor, ElevatorMovementDirection direction)
        {
            if (CanServeFloor(floor))
            {
                state.UnregisterFloorServiceCall(floor, direction);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Addresses target floor request made from within elevator
        /// </summary>
        /// <param name="floor"></param>
        /// <returns></returns>
        public bool MoveTo(int floor)
        {
            if (CanServeFloor(floor))
            {
                state.RegisterTargetFloor(floor);
                return true;
            }
            return false;
        }

        /// <summary>
        /// Addresses undo of target floor request made from within elevator
        /// </summary>
        /// <param name="floor"></param>
        /// <returns></returns>
        public bool UndoMoveTo(int floor)
        {
            if (CanServeFloor(floor))
            {
                state.UnregisterTargetFloor(floor);
                return true;
            }
            return false;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if(!isDisposed)
            {
                stateTransitionManager.Dispose();
                isDisposed = true;
            }
        }

        ~Elevator()
        {
            Dispose(false);
        }
    }
}
