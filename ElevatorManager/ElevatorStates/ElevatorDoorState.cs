﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.ElevatorStates
{
    /// <summary>
    /// Base class for all door movement related states
    /// </summary>
    internal abstract class ElevatorDoorState : ElevatorState
    {
        public const int MAX_DOOR_STAGE = 5;
        public const int MIN_DOOR_STAGE = 1;

        /// <summary>
        /// Multiple stages of door is maintained to enable cancelling of door opening/closing at any moment
        /// When elevator door is fully closed, it reaches <see cref="MAX_DOOR_STAGE"/>. 
        /// Similarly when door is fully opened it reaches <see cref="MIN_DOOR_STAGE"/>. 
        /// </summary>
        protected int doorStage;

        /// <summary>
        /// Time in milliseconds it takes for door to reach a stage while opening or closing the door
        /// </summary>
        protected readonly int doorMovementDurationPerStage;



        protected ElevatorDoorState(ElevatorState previousState, StateTransitionManager stateTransitionManager) : base(previousState, stateTransitionManager)
        {
            doorStage = MAX_DOOR_STAGE;
            doorMovementDurationPerStage = elevator.Configuration.DoorMovementDuration / (MAX_DOOR_STAGE - MIN_DOOR_STAGE + 1);            
        }

        
    }
}
