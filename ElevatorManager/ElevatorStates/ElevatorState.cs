﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.ElevatorStates
{
    /// <summary>
    /// Base class for all elevator state
    /// </summary>
    internal abstract class ElevatorState
    {
        /// <summary>
        /// Direction in which elevator is currently moving
        /// </summary>
        /// <remarks>
        /// When state is Halted, direction may still indicate up/down if elevator is stopping at a floor to address intermediate floor requests
        /// </remarks>
        protected ElevatorMovementDirection direction;

        /// <summary>
        /// Floor number that elevator currently is staying. 
        /// </summary>
        /// <remarks>
        /// When elevator is in moving state, this will indicate previous floor from where it is moving
        /// </remarks>
        protected int currentFloor;

        protected ElevatorStateEnum state;

        /// <summary>
        /// elevator that configuration associated to
        /// </summary>
        protected Elevator elevator;

        protected StateTransitionManager stateTransitionManager;

        protected ElevatorState(Elevator elevator, StateTransitionManager stateTransitionManager)
        {
            Initialize(elevator, stateTransitionManager);
            
        }

        protected ElevatorState(ElevatorState previousState, StateTransitionManager stateTransitionManager)
        {
            Initialize(previousState.elevator, stateTransitionManager);
            CopyData(previousState);
        }

        private void CopyData(ElevatorState previousState)
        {
            direction = previousState.direction;
            currentFloor = previousState.currentFloor;
        }

        private void Initialize(Elevator elevator, StateTransitionManager stateTransitionManager)
        {
            this.elevator = elevator;
            this.stateTransitionManager = stateTransitionManager;
            direction = ElevatorMovementDirection.None;
            currentFloor = elevator.Configuration.MinimumFloor;
        }

        protected abstract void Transition(CancellationToken token);
                
        internal void ContinueTransition(CancellationToken token)
        {
            Transition(token);
        }

        internal virtual ElevatorStatus GetStatus()
        {
            return new ElevatorStatus(elevator.Name, state, currentFloor, direction);
            //return stateTransitionManager.GetStatusSafe(() => { return new ElevatorStatus(elevator.Name, state, currentFloor); });
        }         

        /// <summary>
        /// Initiates door opening process
        /// </summary>
        /// <returns>true if instruction can be accepted as per current state else false</returns>
        public virtual void OpenDoor()
        {
            //do nothing as in most state this is invalid
        }

        /// <summary>
        /// Initiates door closing process
        /// </summary>
        /// <returns></returns>
        public virtual void CloseDoor()
        {
            //do nothing as in most state this is invalid
        }

        /// <summary>
        /// Initiates elevator movement to address floor requests
        /// </summary>
        /// <returns></returns>
        public virtual void MoveToTargetFloor(CancellationToken token)
        {
            //do nothing as in most state this is invalid
        }

        /// <summary>
        /// Registers the floor that passenger had called elevator for
        /// </summary>
        /// <param name="floor"></param>
        public virtual void RegisterFloorServiceCall(int floor, ElevatorMovementDirection directionToServe)
        {
            stateTransitionManager.FloorRequestHandler.RegisterFloorServiceCall(floor, directionToServe);
        }

        /// <summary>
        /// Unregisters the floor that passenger had called elevator for
        /// </summary>
        /// <param name="floor"></param>
        public void UnregisterFloorServiceCall(int floor, ElevatorMovementDirection directionToServe)
        {
            stateTransitionManager.FloorRequestHandler.UnregisterFloorServiceCall(floor, directionToServe);
        }

        /// <summary>
        /// Registers the floor that passenger has selected from within elevator
        /// </summary>
        /// <param name="floor"></param>
        public virtual void RegisterTargetFloor(int floor)
        {
            stateTransitionManager.FloorRequestHandler.RegisterPassengerFloorRequest(floor);
        }

        /// <summary>
        /// Unregisters the floor that passenger has selected from within elevator
        /// </summary>
        /// <param name="floor"></param>
        public void UnregisterTargetFloor(int floor)
        {
            stateTransitionManager.FloorRequestHandler.UnregisterPassengerFloorRequest(floor);
        }





    }
}
