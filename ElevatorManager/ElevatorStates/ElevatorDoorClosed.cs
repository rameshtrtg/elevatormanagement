﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.ElevatorStates
{
    /// <summary>
    /// Represents a state where elevator closed it door and is about to move if any request exists
    /// </summary>
    internal sealed class ElevatorDoorClosed : ElevatorDoorState
    {
        public ElevatorDoorClosed(ElevatorState prevState, StateTransitionManager stateTransitionManager) : base(prevState, stateTransitionManager)
        {
            state = ElevatorStateEnum.DoorClosed;
            doorStage = MAX_DOOR_STAGE;
        }

        public override void RegisterFloorServiceCall(int floor, ElevatorMovementDirection directionToServe)
        {
            base.RegisterFloorServiceCall(floor, directionToServe);
            stateTransitionManager.StartNewTransition(MoveToTargetFloor);
        }

        public override void RegisterTargetFloor(int floor)
        {
            base.RegisterTargetFloor(floor);
            stateTransitionManager.StartNewTransition(MoveToTargetFloor);
        }

        /// <summary>
        /// Initiates elevator movement to address floor requests
        /// </summary>
        /// <returns></returns>
        public override void MoveToTargetFloor(CancellationToken token)
        {
            if (token.IsCancellationRequested || !stateTransitionManager.FloorRequestHandler.HasTargetFloorsToMove())
                return;

            var status = elevator.State.GetStatus();
            var targetFloor = stateTransitionManager.FloorRequestHandler.GetNextFloorRequested(status.Direction, status.CurrentFloor);
            if (targetFloor == FloorRequestHandler.NO_FLOOR_TO_MOVE || status.CurrentFloor == targetFloor)
                return;

            var nextState = new ElevatorMoving(elevator.State, stateTransitionManager);
            stateTransitionManager.SetNextState(nextState, token);
        }

        protected override void Transition(CancellationToken token)
        {            
            if (token.IsCancellationRequested)
                return;            

            MoveToTargetFloor(token);
        }
        
    }
}
