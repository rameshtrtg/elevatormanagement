﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.ElevatorStates
{
    /// <summary>
    /// Represent different state an Elevator will undergo during its lifecycle
    /// </summary>
    public enum ElevatorStateEnum
    {
        /// <summary>
        /// Elevator is under repair
        /// </summary>
        Fault,

        /// <summary>
        /// Elevator stopped at a floor with door closed. 
        /// This may indicate either elevator waiting for service or yet to open the door
        /// </summary>
        Halted, 

        /// <summary>
        /// Elevator has halted at a floor and has door is being opened
        /// </summary>
        DoorOpening,

        /// <summary>
        /// Elevator has halted at a floor and has completed opening the door. 
        /// It is waiting for people enter the lift
        /// </summary>
        DoorOpened,

        /// <summary>
        /// Once the door is openened, lift will wait for passenger to move in/out for configured time before start closing
        /// </summary>
        WaitingForPassenger,

        /// <summary>
        /// Elevator has halted at a floor and post wait time after door opened, started closing door
        /// </summary>
        DoorClosing,

        /// <summary>
        /// Elevator has halted at a floor and completed closing door and waiting for sometime before moving to get any 'door open' instruction
        /// </summary>
        DoorClosed,

        /// <summary>
        /// Elevator is moving between floors
        /// </summary>
        Moving,
        
    }
}
