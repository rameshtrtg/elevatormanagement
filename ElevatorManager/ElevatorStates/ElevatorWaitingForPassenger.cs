﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.ElevatorStates
{
    /// <summary>
    ///  Represents state where elevator is waiting for passenger movement
    /// </summary>
    internal class ElevatorWaitingForPassenger : ElevatorDoorState
    {
        protected const int MIN_PASSENGER_WAIT_STAGE = 1;
        protected const int MAX_PASSENGER_WAIT_STAGE = 3;

        /// <summary>
        /// Multiple stages of elevator waiting for passenger is maintained to enable door closing at any moment 
        /// </summary>
        private int passengerWaitStage;

        /// <summary>
        /// Time in milliseconds it takes for elevator to reach a stage while waiting for passenger
        /// </summary>
        private readonly int passengerMovementDurationPerStage;

        public ElevatorWaitingForPassenger(ElevatorState previousState, StateTransitionManager stateTransitionManager) : base(previousState, stateTransitionManager)
        {
            passengerWaitStage = MIN_PASSENGER_WAIT_STAGE;
            passengerMovementDurationPerStage = elevator.Configuration.PassengerMovementDuration / (MAX_PASSENGER_WAIT_STAGE - MIN_PASSENGER_WAIT_STAGE + 1);
            state = ElevatorStateEnum.WaitingForPassenger;
            doorStage = MIN_DOOR_STAGE;
        }

        protected override void Transition(CancellationToken token)
        {

            if (token.IsCancellationRequested)
                return;
            if (WaitForPassengerMovement(token))
            {
                var nextState = new ElevatorDoorClosing(this, stateTransitionManager, MIN_DOOR_STAGE);
                stateTransitionManager.SetNextState(nextState, token);
            }
        }

        /// <summary>
        /// Waits for passenger in background thread and once wait time ends, initiates closing of door
        /// </summary>
        private bool WaitForPassengerMovement(CancellationToken token)
        {
            while (!token.IsCancellationRequested && passengerWaitStage < MAX_PASSENGER_WAIT_STAGE)
            {
                Thread.Sleep(passengerMovementDurationPerStage);
                passengerWaitStage++;
            }
            passengerWaitStage = MIN_PASSENGER_WAIT_STAGE;
            if (token.IsCancellationRequested)
                return false;
            return true;
        }
    }
}
