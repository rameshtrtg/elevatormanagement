﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.ElevatorStates
{
    /// <summary>
    /// Represents elevator door opening state which happens after elevator reaching a floor where it was requested to serve
    /// </summary>
    internal sealed class ElevatorDoorOpening : ElevatorDoorState
    {

        public ElevatorDoorOpening(ElevatorState previousState, StateTransitionManager stateTransitionManager) : base(previousState, stateTransitionManager)
        {
            state = ElevatorStateEnum.DoorOpening;
        }

        public ElevatorDoorOpening(ElevatorState previousState, StateTransitionManager stateTransitionManager, int doorStage) : base(previousState, stateTransitionManager)
        {
            state = ElevatorStateEnum.DoorOpening;
            this.doorStage = doorStage;
        }

        /// <summary>
        /// Performs the door opening operation in background thread and updates the state accordingly
        /// </summary>
        protected override void Transition(CancellationToken token)
        {
            
            while (!token.IsCancellationRequested && doorStage > MIN_DOOR_STAGE)
            {
                Thread.Sleep(doorMovementDurationPerStage);
                doorStage--;
            }

            if (doorStage == MIN_DOOR_STAGE)
            {
                var nextState = new ElevatorDoorOpened(this, stateTransitionManager, doorStage);
                stateTransitionManager.SetNextState(nextState, token);
            }
            
        }

        
    }
}
