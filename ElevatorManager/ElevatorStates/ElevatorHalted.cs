﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.ElevatorStates
{
    /// <summary>
    /// Represents elevator being stopped after movement and has no other request to address
    /// </summary>
    internal sealed class ElevatorHalted : ElevatorState
    {
        private int targetFloor;
        public ElevatorHalted(ElevatorState prevState, StateTransitionManager stateTransitionManager, int targetFloor) : base(prevState, stateTransitionManager)
        {
            this.targetFloor = targetFloor;
            state = ElevatorStateEnum.Halted;
        }

        public ElevatorHalted(Elevator elevator, StateTransitionManager stateTransitionManager) :base(elevator, stateTransitionManager)
        {
            targetFloor = currentFloor;
            state = ElevatorStateEnum.Halted;
        }

        public override void OpenDoor()
        {
            stateTransitionManager.StopCurrentAndStartNewTransition(InnerOpenDoor);
        }

        private void InnerOpenDoor(CancellationToken token)
        {
            if (token.IsCancellationRequested)
                return;
            var nextState = new ElevatorDoorOpening(elevator.State, stateTransitionManager);
            stateTransitionManager.SetNextState(nextState, token);
        }

        /// <summary>
        /// Initiates elevator movement to address floor requests
        /// </summary>
        /// <returns></returns>
        public override void MoveToTargetFloor(CancellationToken token)
        {
            if (!stateTransitionManager.FloorRequestHandler.HasTargetFloorsToMove())
                return;

            var status = elevator.State.GetStatus();
            targetFloor = stateTransitionManager.FloorRequestHandler.GetNextFloorRequested(status.Direction, status.CurrentFloor);
            if (targetFloor == FloorRequestHandler.NO_FLOOR_TO_MOVE || status.CurrentFloor == targetFloor)
                return;

            var nextState = new ElevatorMoving(elevator.State, stateTransitionManager);
            stateTransitionManager.SetNextState(nextState, token);
        }

        public override void RegisterFloorServiceCall(int floor, ElevatorMovementDirection directionToServe)
        {
            base.RegisterFloorServiceCall(floor, directionToServe);
            stateTransitionManager.StartNewTransition(MoveToTargetFloor);
        }

        public override void RegisterTargetFloor(int floor)
        {
            base.RegisterTargetFloor(floor);
            stateTransitionManager.StartNewTransition(MoveToTargetFloor);
        }

        protected override void Transition(CancellationToken token)
        {
            
            if (token.IsCancellationRequested)
                return;

            if (currentFloor == targetFloor)
            {
                UnregisterTargetFloor(currentFloor);
                InnerOpenDoor(token);
            }
            else
            {
                UnregisterFloorServiceCall(currentFloor, direction);
                UnregisterTargetFloor(currentFloor);
                
                MoveToTargetFloor(token);
            }

        }
    }
}
