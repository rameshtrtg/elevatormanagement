﻿using Eurofins.Infrastructure.Threading;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.ElevatorStates
{
    /// <summary>
    /// StateTransitionManager facilitates transition from one state to other.
    /// </summary>
    /// <remarks>
    /// Each state is represented with its own class and has logic to transition to next possible state.
    /// StateTransitionManager facilitates to move between the states
    /// </remarks>
    internal class StateTransitionManager : IDisposable
    {
        private bool isDisposed;

        /// <summary>
        /// Represents the operation that the lift currently performing
        /// </summary>
        protected Task currentOperation;

        /// <summary>
        /// The task representing currentOperation will make write lock to update elevator status when it is running
        /// </summary>
        private readonly ReaderWriterLockSlim stateLock;

        private readonly Elevator elevator;

        private readonly SequentialTaskExecuter taskExecutor;

        public StateTransitionManager(Elevator elevator)
        {
            taskExecutor = new SequentialTaskExecuter();
            isDisposed = false;
            this.elevator = elevator;
            FloorRequestHandler = new FloorRequestHandler(elevator.Configuration.MinimumFloor, elevator.Configuration.MaximumFloor);
            stateLock = new ReaderWriterLockSlim();
        }

        /// <summary>
        /// Handler for managing floor requests and suggest next floor that elevator can move
        /// </summary>
        public FloorRequestHandler FloorRequestHandler { get; }

        /// <summary>
        /// This method ensures that at any given time only requested task runs. 
        /// If any existing task is already running, it cancels it before starting the current one.
        /// </summary>
        /// <param name="transitionAction"></param>
        public void StopCurrentAndStartNewTransition(Action<CancellationToken> transitionAction)
        {
            taskExecutor.CancelAllAndExecute((token) => TaskToRun(transitionAction, token));
        }
        /// <summary>
        /// This method ensures that it runs the new transition after currently running one (if any). 
        /// </summary>
        /// <param name="transitionAction"></param>
        public void StartNewTransition(Action<CancellationToken> transitionAction)
        {
            taskExecutor.Execute((token) => TaskToRun(transitionAction, token));
        }

        private void TaskToRun(Action<CancellationToken> transitionAction, CancellationToken token)
        {
            Thread.CurrentThread.IsBackground = true;
            stateLock.EnterWriteLock();
            try
            {
                transitionAction(token);
            }
            finally
            {
                stateLock.ExitWriteLock();
            }
        }


        public void SetNextState(ElevatorState nextState, CancellationToken token)
        {
            elevator.State = nextState;
            Thread.Sleep(10);
            nextState.ContinueTransition(token);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!isDisposed)
            {
                stateLock.Dispose();
                taskExecutor.Shutdown();
                FloorRequestHandler.Dispose();
                isDisposed = true;
            }
        }

        ~StateTransitionManager()
        {
            Dispose(false);
        }
    }
}
