﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.ElevatorStates
{
    /// <summary>
    /// Represents elevator door opened state which happens after full opening of door and about to wait for passenger movement 
    /// </summary>
    internal sealed class ElevatorDoorOpened : ElevatorDoorState
    {       

        public ElevatorDoorOpened(ElevatorState prevState, StateTransitionManager stateTransitionManager, int doorStage) : base(prevState, stateTransitionManager)
        {            
            state = ElevatorStateEnum.DoorOpened;
            this.doorStage = doorStage;
        }

        protected override void Transition(CancellationToken token)
        {            
            if (token.IsCancellationRequested)
                return;
            var nextState = new ElevatorWaitingForPassenger(this, stateTransitionManager);
            stateTransitionManager.SetNextState(nextState, token);
            
        }
        
    }
}
