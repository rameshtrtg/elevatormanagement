﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.ElevatorStates
{
    /// <summary>
    ///  Represents elevator moving state which happens after door is closed or 
    ///  in halted state when a request to move was received
    /// </summary>
    internal sealed class ElevatorMoving : ElevatorState
    {
        private readonly int floorMovementDuration;

        public ElevatorMoving(ElevatorState prevState, StateTransitionManager stateTransitionManager) : base(prevState, stateTransitionManager)
        {
            floorMovementDuration = elevator.Configuration.FloorMovementDuration;
            state = ElevatorStateEnum.Moving;
        }

        protected override void Transition(CancellationToken token)
        {
            if (token.IsCancellationRequested)
                return;

            var targetFloor = stateTransitionManager.FloorRequestHandler.GetNextFloorRequested(direction, currentFloor);
            direction = GetDirection(targetFloor);

            if (targetFloor == FloorRequestHandler.NO_FLOOR_TO_MOVE)
                return;

            if (direction != ElevatorMovementDirection.None)
                UnregisterFloorServiceCall(currentFloor, direction);

            var floorInMovement = currentFloor;
            while (CanMoveFurther(token, floorInMovement, targetFloor))
            {
                Thread.Sleep(floorMovementDuration);
                direction = GetDirection(targetFloor);
                targetFloor = stateTransitionManager.FloorRequestHandler.GetNextFloorRequested(direction, floorInMovement);
                floorInMovement = GetFloorInMovement(floorInMovement);
                currentFloor = floorInMovement;
            }

            var nextState = new ElevatorHalted(this, stateTransitionManager, targetFloor);
            stateTransitionManager.SetNextState(nextState, token);

        }

        private ElevatorMovementDirection GetDirection(int targetFloor)
        {
            if (targetFloor == FloorRequestHandler.NO_FLOOR_TO_MOVE || currentFloor == targetFloor)
                return ElevatorMovementDirection.None;
            return (currentFloor > targetFloor) ? ElevatorMovementDirection.Down : ElevatorMovementDirection.Up;
        }

        /// <summary>
        /// When elevator is in movement, it returns its currently visting floor (i.e., when it has reached a floor which is not its target)
        /// </summary>
        /// <param name="currentFloor"></param>
        /// <returns></returns>
        private int GetFloorInMovement(int currentFloor)
        {
            return (direction == ElevatorMovementDirection.Down) ? currentFloor - 1 : currentFloor + 1;
        }

        private bool CanMoveFurther(CancellationToken token, int floorInMovement, int targetFloor)
        {
            if (token.IsCancellationRequested || targetFloor == FloorRequestHandler.NO_FLOOR_TO_MOVE || floorInMovement == targetFloor)
                return false;
            switch (direction)
            {
                case ElevatorMovementDirection.Up:
                    return floorInMovement < targetFloor;
                case ElevatorMovementDirection.Down:
                    return floorInMovement > targetFloor;
                default:
                    return true;
            }


        }


    }
}
