﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.ElevatorStates
{
    /// <summary>
    /// Represents elevator door closing state which happens after waiting for passenger movement 
    /// </summary>
    internal sealed class ElevatorDoorClosing : ElevatorDoorState
    {
        public ElevatorDoorClosing(ElevatorState previousState, StateTransitionManager stateTransitionManager, int doorStage) :base(previousState, stateTransitionManager)
        {
            state = ElevatorStateEnum.DoorClosing;
            this.doorStage = doorStage;
        }

        public override void OpenDoor()
        {
            stateTransitionManager.StopCurrentAndStartNewTransition(InnerOpenDoor);
        }

        private void InnerOpenDoor(CancellationToken token)
        {            
            if (token.IsCancellationRequested)
                return;
            var status = elevator.State.GetStatus();
            var nextState = new ElevatorDoorOpening(elevator.State, stateTransitionManager, status.DoorStage);            
            stateTransitionManager.SetNextState(nextState, token);
        }

        internal override ElevatorStatus GetStatus()
        {
            return new ElevatorStatus(elevator.Name, state, currentFloor, direction, doorStage);
        }

        protected override void Transition(CancellationToken token)
        {            
            while (!token.IsCancellationRequested && doorStage < MAX_DOOR_STAGE)
            {
                Thread.Sleep(doorMovementDurationPerStage);
                doorStage++;
            }

            if (doorStage == MAX_DOOR_STAGE)
            {                
                var nextState = new ElevatorDoorClosed(this, stateTransitionManager);
                stateTransitionManager.SetNextState(nextState, token);                
            }
        }
        
    }
}
