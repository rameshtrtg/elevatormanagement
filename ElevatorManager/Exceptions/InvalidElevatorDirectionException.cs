﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.Exceptions
{
    [Serializable]
    public class InvalidElevatorDirectionException : Exception
    {
        
        public InvalidElevatorDirectionException(string message) 
            : base(message)
        {
        }

        public InvalidElevatorDirectionException(string message, Exception innerException) 
            : base(message, innerException)
        {
        }
        
        protected InvalidElevatorDirectionException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
        }
    }
}
