﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager.Exceptions
{
    [Serializable]
    public class InvalidFloorException: Exception
    {
        
        public InvalidFloorException(string message) 
            : base(message)
        {
        }

        public InvalidFloorException(string message, Exception innerException) 
            : base(message, innerException)
        {
        }
        
        protected InvalidFloorException(SerializationInfo info, StreamingContext context) 
            : base(info, context)
        {
        }
    }
}
