﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eurofins.ElevatorManager
{
    /// <summary>
    /// Represents elevator movement direction
    /// </summary>
    public enum ElevatorMovementDirection
    {
        /// <summary>
        /// List is halted and has no request to address
        /// </summary>
        None,
        /// <summary>
        /// Lift is moving up
        /// </summary>
        Up,
        /// <summary>
        /// Lift is moving down
        /// </summary>
        Down
    }
}
