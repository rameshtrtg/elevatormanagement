﻿using Mediator.Net.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mediator.Net.Context;
using System.Threading;
using Unity;
using Eurofins.ElevatorManager.ElevatorStates;

namespace Eurofins.ElevatorManager
{
    /// <summary>
    /// ElevatorSynchronizer receives all the elevator requests and decides the right elevator to service the request.
    /// It tracks the movement of the elevator and revisits the request alocation already made to ensure fast response for the requests
    /// </summary>
    public class ElevatorSynchronizer : IFloorServiceRequest, IEventHandler<ElevatorStateChangedEvent>, IDisposable
    {
        private bool isDisposed;
        private readonly Dictionary<string, Elevator> elevators;
        private readonly ElevatorConfiguration configuration;
        private readonly FloorServiceCallAllocation floorServiceCallAllocation;

        public ElevatorSynchronizer(List<string> elevatorNames, ElevatorConfiguration config)
        {
            if (elevatorNames == null || elevatorNames.Count == 0)
                throw new ArgumentNullException(nameof(elevatorNames), "Atleast one elevator name is expected");
            if (config == null)
                throw new ArgumentNullException(nameof(config), "Configuration cannot be null");

            isDisposed = false;
            elevators = new Dictionary<string, Elevator>();
            configuration = config;
            var elevatorList = new List<Elevator>();
            foreach (var name in elevatorNames)
            {
                var elevator = new Elevator(name, config);
                elevators.Add(name, elevator);
                elevatorList.Add(elevator);
            }
            floorServiceCallAllocation = new FloorServiceCallAllocation(elevatorList, config.MinimumFloor, config.MaximumFloor);
        }


        private bool CanServeFloor(int floor)
        {
            return (floor >= configuration.MinimumFloor && floor <= configuration.MaximumFloor);
        }

        public bool ServeFloor(int floor, ElevatorMovementDirection direction)
        {
            if (!CanServeFloor(floor))
                return false;

            return floorServiceCallAllocation.AllocateElevator(new FloorServiceCall(floor, direction));

        }

        public bool CancelFloorService(int floor, ElevatorMovementDirection direction)
        {
            if (!CanServeFloor(floor))
                return false;
            return floorServiceCallAllocation.DeallocateElevator(new FloorServiceCall(floor, direction));

        }

        public void MoveTo(string elevatorName, int floor)
        {
            if (elevators.ContainsKey(elevatorName) && CanServeFloor(floor))
            {
                elevators[elevatorName].MoveTo(floor);
            }
        }

        public Task Handle(IReceiveContext<ElevatorStateChangedEvent> context, CancellationToken cancellationToken)
        {
            return Task.Run(() =>
            {
                var status = context.Message.ElevatorStatus;
                if (status.State == ElevatorStateEnum.Halted || status.State == ElevatorStateEnum.DoorClosed)
                {
                    floorServiceCallAllocation.RevisitAllocation(elevators[status.ElevatorName]);
                }
            });
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!isDisposed)
            {
                foreach (var elevator in elevators.Values)
                {
                    elevator.Dispose();
                }
                if (isDisposing)
                {
                    elevators.Clear();
                }
                isDisposed = true;
            }
        }



        ~ElevatorSynchronizer()
        {
            Dispose(false);
        }
    }
}
