**Projects in Solution**

ElevatorManagerClient - This is the project to see the usage of the ElevatorManager library. 
						Program.cs represents the elevator servicing situation mentioned in the Assessment document. 
						Project also includes a sample Audit class to showcase how status can be tracked by just plugging 
						in the code from outside ElevatorManager project
						
						
Infrastructure - Provides basic infrastructure to commnucate between components


ElevatorManager - This is the class library that maintains code for elevator request management


ElevatorManagerTests - Test project for ElevatorManager. Though very few scenarios are tested, it is kept to see samples for test code

---

## Patterns Used

1. Builder pattern to accept various configuration and generation a synchronizer object finally.
2. State pattern to represent each state of elevator and transition properly
3. Pattern for Disposing and thread management used

---

## Other Aspects of code

1. Code quality maintained by following SOLID principles
2. Code Comments provided
3. Some custom exceptions provided
4. Dependency injection done using Unity framework
5. Mediator.Net is used to loose couple components and also facilitate a pluggable mechanish to listen to state changes 
6. Code is written in such a way that elevator can be either directly be requested ro through synchronizer when multile elevators exists.
