﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorManagerTests
{
    public static class MemberAccessProvider
    {
        public static TR GetPrivatePropertyValue<T, TR>(T owner, string propertyName)
        {
            var ownerType = typeof(T);
            var field = ownerType.GetField(propertyName, BindingFlags.NonPublic | BindingFlags.Instance);
            return (TR)field.GetValue(owner);
        }

        public static void SetPrivatePropertyValue<T, TR>(T owner, string propertyName, TR value)
        {
            var ownerType = typeof(T);
            var field = ownerType.GetField(propertyName, BindingFlags.NonPublic | BindingFlags.Instance);
            field.SetValue(owner, value);
        }
    }
}
