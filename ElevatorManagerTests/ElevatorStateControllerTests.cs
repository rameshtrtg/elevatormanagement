﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Eurofins.ElevatorManager;
using Moq;
using Mediator.Net;
using System.Threading;
using System.Threading.Tasks;
using Eurofins.ElevatorManager.ElevatorStates;
using Eurofins.Infrastructure.Threading;

namespace ElevatorManagerTests
{
    [TestClass]
    public class ElevatorStateControllerTests
    {

        private ElevatorConfiguration config;
        private Elevator elevator;
        private StateTransitionManager stateTransitionManager;
        Mock<IMediator> mediatorMock;


        [TestInitialize]
        public void SetupTest()
        {
            config = new ElevatorConfiguration(10, 10, 10, 0, 2);
            elevator = new Elevator("E1", config);
            stateTransitionManager = MemberAccessProvider.GetPrivatePropertyValue<Elevator, StateTransitionManager>(elevator, "stateTransitionManager");

            SetupMediator();
        }

        [TestMethod]
        public void When_Elevator_At_Ground_Floor_Passenger_Calls_From_First_Floor_To_Go_Up_Elevator_Should_Reach_Open_And_Close_Door()
        {

            elevator.ServeFloor(1, ElevatorMovementDirection.Up);

            WaitForElevatorOperationToComplete();
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 0 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Moving), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Halted), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpening), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpened), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.WaitingForPassenger), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosing), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosed), It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public void When_Elevator_At_Ground_Floor_Passenger_Calls_From_First_Floor_To_Go_Down_Elevator_Should_Reach_Open_And_Close_Door()
        {

            elevator.ServeFloor(1, ElevatorMovementDirection.Down);

            WaitForElevatorOperationToComplete();
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 0 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Moving), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Halted), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpening), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpened), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.WaitingForPassenger), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosing), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosed), It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public void When_Elevator_At_Ground_Floor_Passenger_Calls_From_First_Floor_To_Go_Up_And_Second_Floor_To_Go_Down_Elevator_Should_Reach_Open_And_Close_Door()
        {

            elevator.ServeFloor(1, ElevatorMovementDirection.Up);
            elevator.ServeFloor(2, ElevatorMovementDirection.Down);

            WaitForElevatorOperationToComplete();
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 0 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Moving), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Halted), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpening), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpened), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.WaitingForPassenger), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosing), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosed), It.IsAny<CancellationToken>()), Times.Once);

            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Moving), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Halted), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpening), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpened), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.WaitingForPassenger), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosing), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosed), It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public void When_Elevator_At_Ground_Floor_Passenger_Calls_From_First_Floor_Multiple_Floor_Calls_Arrives()
        {

            elevator.ServeFloor(1, ElevatorMovementDirection.Up);
            elevator.ServeFloor(2, ElevatorMovementDirection.Down);
            elevator.ServeFloor(0, ElevatorMovementDirection.Up);

            WaitForElevatorOperationToComplete();
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 0 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Moving), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Halted), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpening), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpened), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.WaitingForPassenger), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosing), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosed), It.IsAny<CancellationToken>()), Times.Once);

            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Moving), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Halted), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpening), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpened), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.WaitingForPassenger), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosing), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosed), It.IsAny<CancellationToken>()), Times.Once);

            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Moving), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 0 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Halted), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 0 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpening), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 0 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpened), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 0 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.WaitingForPassenger), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 0 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosing), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 0 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosed), It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public void When_Elevator_At_Second_Floor_Passenger_Calls_From_First_Floor_To_Go_Down_Elevator_Should_Reach_Open_And_Close_Door()
        {
            elevator.ServeFloor(2, ElevatorMovementDirection.Down);
            WaitForElevatorOperationToComplete();
            SetupMediator();

            elevator.ServeFloor(1, ElevatorMovementDirection.Down);

            WaitForElevatorOperationToComplete();
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Moving), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Halted), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpening), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpened), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosing), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosed), It.IsAny<CancellationToken>()), Times.Once);
        }

        [TestMethod]
        public void When_Elevator_At_Second_Floor_Passenger_Calls_From_First_Floor_To_Go_Up_Elevator_Should_Reach_Open_And_Close_Door()
        {
            elevator.ServeFloor(2, ElevatorMovementDirection.Down);
            WaitForElevatorOperationToComplete();
            SetupMediator();

            elevator.ServeFloor(1, ElevatorMovementDirection.Up);

            WaitForElevatorOperationToComplete();
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 2 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Moving), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.Halted), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpening), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorOpened), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosing), It.IsAny<CancellationToken>()), Times.Once);
            mediatorMock.Verify(x => x.PublishAsync(It.Is<ElevatorStateChangedEvent>(statusEvent => statusEvent.ElevatorStatus.CurrentFloor == 1 && statusEvent.ElevatorStatus.State == ElevatorStateEnum.DoorClosed), It.IsAny<CancellationToken>()), Times.Once);
        }

        private void WaitForElevatorOperationToComplete()
        {            
            //var taskExecutor = MemberAccessProvider.GetPrivatePropertyValue<StateTransitionManager, SequentialTaskExecuter>(stateTransitionManager, "taskExecutor");
            //while (!taskExecutor.IsShutdown)
                Thread.Sleep(2000);
        }

        private void SetupMediator()
        {
            mediatorMock = new Mock<IMediator>();
            mediatorMock.Setup(x => x.PublishAsync(It.IsAny<ElevatorStateChangedEvent>(), It.IsAny<CancellationToken>()));
            var mediator = new Lazy<IMediator>(() => { return mediatorMock.Object; });
            MemberAccessProvider.SetPrivatePropertyValue(Eurofins.Infrastructure.Environment.Instance, "mediator", mediator);
        }

    }
}
