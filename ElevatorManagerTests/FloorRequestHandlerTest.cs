﻿using Eurofins.ElevatorManager;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ElevatorManagerTests
{
    [TestClass]
    public class FloorRequestHandlerTest
    {

        [TestInitialize]
        public void SetupTest()
        {

        }


        #region Floor Request Made within Elevator
        [TestMethod]
        public void When_Elevator_Reached_Up_To_Floor_9_And_All_Are_Lower_Floor_Request()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterPassengerFloorRequest(5);
            target.RegisterPassengerFloorRequest(3);
            target.RegisterPassengerFloorRequest(-2);
            target.RegisterPassengerFloorRequest(8);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Up, 9);

            Assert.AreEqual(8, result);
        }

        [TestMethod]
        public void When_Elevator_Reached_Down_To_Floor_9_And_All_Are_Lower_Floor_Request()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterPassengerFloorRequest(5);
            target.RegisterPassengerFloorRequest(3);
            target.RegisterPassengerFloorRequest(-2);
            target.RegisterPassengerFloorRequest(8);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Down, 9);

            Assert.AreEqual(8, result);
        }

        [TestMethod]
        public void When_Elevator_Halted_At_Floor_9_And_All_Are_Lower_Floor_Request()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterPassengerFloorRequest(5);
            target.RegisterPassengerFloorRequest(3);
            target.RegisterPassengerFloorRequest(-2);
            target.RegisterPassengerFloorRequest(8);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.None, 9);

            Assert.AreEqual(8, result);
        }

        [TestMethod]
        public void When_Elevator_Reached_Up_To_Floor_0_And_All_Are_Higher_Floor_Request()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterPassengerFloorRequest(5);
            target.RegisterPassengerFloorRequest(3);
            target.RegisterPassengerFloorRequest(10);
            target.RegisterPassengerFloorRequest(8);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Up, 0);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void When_Elevator_Reached_Down_To_Floor_0_And_All_Are_Higher_Floor_Request()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterPassengerFloorRequest(5);
            target.RegisterPassengerFloorRequest(3);
            target.RegisterPassengerFloorRequest(10);
            target.RegisterPassengerFloorRequest(8);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Down, 0);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void When_Elevator_Halted_At_Floor_0_And_All_Are_Higher_Floor_Request()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterPassengerFloorRequest(5);
            target.RegisterPassengerFloorRequest(3);
            target.RegisterPassengerFloorRequest(10);
            target.RegisterPassengerFloorRequest(8);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.None, 0);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void When_Elevator_Reached_Up_To_Floor_5_And_Have_Mixed_Floor_Request()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterPassengerFloorRequest(-2);
            target.RegisterPassengerFloorRequest(3);
            target.RegisterPassengerFloorRequest(2);
            target.RegisterPassengerFloorRequest(10);
            target.RegisterPassengerFloorRequest(8);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Up, 5);

            Assert.AreEqual(8, result);

        }

        [TestMethod]
        public void When_Elevator_Reached_Down_To_Floor_5_And_Have_Mixed_Floor_Request()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterPassengerFloorRequest(-2);
            target.RegisterPassengerFloorRequest(3);
            target.RegisterPassengerFloorRequest(2);
            target.RegisterPassengerFloorRequest(10);
            target.RegisterPassengerFloorRequest(8);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Down, 5);

            Assert.AreEqual(3, result);

        }

        [TestMethod]
        public void When_Elevator_Halted_At_Floor_5_And_Have_Mixed_Floor_Request()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterPassengerFloorRequest(-2);
            target.RegisterPassengerFloorRequest(3);
            target.RegisterPassengerFloorRequest(2);
            target.RegisterPassengerFloorRequest(10);
            target.RegisterPassengerFloorRequest(8);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.None, 5);

            Assert.AreEqual(3, result);

        }

        #endregion

        #region FloorServiCall

        [TestMethod]
        public void When_Elevator_Reached_Up_To_Floor_9_And_All_Are_Lower_Floor_ServiceCall()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterFloorServiceCall(5, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(3, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(-2, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(8, ElevatorMovementDirection.Up);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Up, 9);

            Assert.AreEqual(8, result);
        }

        [TestMethod]
        public void When_Elevator_Reached_Down_To_Floor_9_And_All_Are_Lower_Floor_ServiceCall()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterFloorServiceCall(5, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(3, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(-2, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(8, ElevatorMovementDirection.Up);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Down, 9);

            Assert.AreEqual(8, result);
        }

        [TestMethod]
        public void When_Elevator_Halted_At_Floor_9_And_All_Are_Lower_Floor_ServiceCall()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterFloorServiceCall(5, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(3, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(-2, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(8, ElevatorMovementDirection.Up);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.None, 9);

            Assert.AreEqual(8, result);
        }

        [TestMethod]
        public void When_Elevator_Reached_Up_To_Floor_0_And_All_Are_Higher_Floor_ServiceCall()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterFloorServiceCall(5, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(3, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(10, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(8, ElevatorMovementDirection.Up);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Up, 0);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void When_Elevator_Reached_Down_To_Floor_0_And_All_Are_Higher_Floor_ServiceCall()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterFloorServiceCall(5, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(3, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(10, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(8, ElevatorMovementDirection.Up);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Down, 0);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void When_Elevator_Halted_At_Floor_0_And_All_Are_Higher_Floor_ServiceCall()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterFloorServiceCall(5, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(3, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(10, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(8, ElevatorMovementDirection.Up);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.None, 0);

            Assert.AreEqual(3, result);
        }

        [TestMethod]
        public void When_Elevator_Reached_Up_To_Floor_5_And_Have_Mixed_Floor_ServiceCall()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterFloorServiceCall(-2, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(3, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(2, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(10, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(8, ElevatorMovementDirection.Up);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Up, 5);

            Assert.AreEqual(8, result);

        }

        [TestMethod]
        public void When_Elevator_Reached_Down_To_Floor_5_And_Have_Mixed_Floor_ServiceCall()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterFloorServiceCall(-2, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(3, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(2, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(10, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(8, ElevatorMovementDirection.Up);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.Down, 5);

            Assert.AreEqual(3, result);

        }

        [TestMethod]
        public void When_Elevator_Halted_At_Floor_5_And_Have_Mixed_Floor_ServiceCall()
        {
            var target = new FloorRequestHandler(-2, 10);
            target.RegisterFloorServiceCall(-2, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(3, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(2, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(10, ElevatorMovementDirection.Up);
            target.RegisterFloorServiceCall(8, ElevatorMovementDirection.Up);

            var result = target.GetNextFloorRequested(ElevatorMovementDirection.None, 5);

            Assert.AreEqual(3, result);

        }
        #endregion
    }
}
