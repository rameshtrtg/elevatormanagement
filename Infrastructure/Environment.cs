﻿using Mediator.Net;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using CommonServiceLocator;
using Unity;

namespace Eurofins.Infrastructure
{
    public class Environment
    {
        private readonly Lazy<IMediator> mediator = new Lazy<IMediator>(CreateMediator);       
       
        private static Lazy<Environment> singleton = new Lazy<Environment>(()=>new Environment());


        private Environment()
        {
            UnityContainer = new UnityContainer();
            UnityContainer.RegisterInstance<ISequenceGenerator>(new SequenceGenerator());

        }

        public static Environment Instance { get { return singleton.Value; } }

        public IMediator Mediator { get { return mediator.Value; } }        

        private static IMediator CreateMediator()
        {
            var mediaBuilder = new MediatorBuilder();
            foreach (var assembly in GetAssemblies())
                mediaBuilder.RegisterHandlers(assembly);
            return mediaBuilder.Build();
        }

        private static IEnumerable<Assembly> GetAssemblies()
        {
            return AppDomain.CurrentDomain.GetAssemblies().Where(x => x.FullName.Substring(0, 8) == "Eurofins");
        }

        public UnityContainer UnityContainer { get; }

    }
}
