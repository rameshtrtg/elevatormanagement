﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Eurofins.Infrastructure
{
    internal class SequenceGenerator : ISequenceGenerator
    {
        private int counter;
        public SequenceGenerator()
        {
            counter = 0;
        }
        public int GetNext()
        {
            if (counter == int.MaxValue)
                counter = 0;
            return ++counter;
        }
    }
}
