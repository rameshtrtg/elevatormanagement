﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Eurofins.Infrastructure.Threading
{
    /// <summary>
    /// SequentialTaskExecuter queues all tasks but executes one task at a time. It ensures running task is completed before starting the next
    /// It provides an option to cancel all pending tasks before executing new one.
    /// </summary>
    public class SequentialTaskExecuter : IDisposable
    {

        private bool isDisposed;
        private BlockingCollection<Task> queue;
        private readonly TimeSpan TakeFrequency = TimeSpan.FromSeconds(2);

        /// <summary>
        /// represents whether the Executor is shutdown or not
        /// </summary>
        private bool isShutdown;
        private readonly CancellationTokenSource executerCancellationTokenSource;
        private readonly Task threadExecuter;

        private CancellationTokenSource taskCancellationTokenSource;
        private Task currentlyRunningTask;

        /// <summary>
        /// Instantiates QueuedTaskExecuter
        /// </summary>        
        public SequentialTaskExecuter()
        {
            isDisposed = false;
            queue = new BlockingCollection<Task>();
            isShutdown = false;
            executerCancellationTokenSource = new CancellationTokenSource();
            taskCancellationTokenSource = new CancellationTokenSource();
            currentlyRunningTask = null;
            threadExecuter = Task.Run(() => RunPool(executerCancellationTokenSource.Token), executerCancellationTokenSource.Token);
        }

        /// <summary>
        /// This is the actual executer task which runs all the tasks in queue one after the other (not in parallel)
        /// </summary>
        /// <param name="token"></param>
        private void RunPool(CancellationToken token)
        {
            Thread.CurrentThread.IsBackground = true;

            while (!token.IsCancellationRequested)
            {
                Task taskInQueue;
                if (queue.TryTake(out taskInQueue, TakeFrequency))
                {
                    if (currentlyRunningTask != null)
                    {
                        Task.WaitAny(currentlyRunningTask);
                        currentlyRunningTask = null;
                    }
                    if (!taskInQueue.IsCanceled)
                    {
                        currentlyRunningTask = taskInQueue;
                        taskInQueue.Start();
                    }
                }
            }
        }

        /// <summary>
        /// Creates and Submits a task to the queue which will be executer later
        /// </summary>
        /// <param name="runnable"></param>
        /// <returns></returns>
        public void Execute(Action<CancellationToken> action)
        {
            if (isShutdown)
                throw new InvalidOperationException("Submit called after executor shutdown");
            queue.Add(new Task(() => action(taskCancellationTokenSource.Token), taskCancellationTokenSource.Token));
        }

        /// <summary>
        /// Creates and Submits a task to the queue which will be executer later
        /// </summary>
        /// <param name="runnable"></param>
        /// <returns></returns>
        public void CancelAllAndExecute(Action<CancellationToken> action)
        {
            if (isShutdown)
                throw new InvalidOperationException("Submit called after executor shutdown");
            taskCancellationTokenSource.Cancel();
            if (currentlyRunningTask != null)
                Task.WaitAny(currentlyRunningTask);
            taskCancellationTokenSource.Dispose();
            taskCancellationTokenSource = new CancellationTokenSource();
            queue.Add(new Task(() => action(taskCancellationTokenSource.Token), taskCancellationTokenSource.Token));
        }

        /// <summary>
        /// Shutsdown SequentialTaskExecuter
        /// </summary>
        public void Shutdown()
        {
            if (isShutdown)
                return;
            StopAllExecutions();
            Dispose();
        }

        private void StopAllExecutions()
        {
            taskCancellationTokenSource.Cancel();
            executerCancellationTokenSource.Cancel();
            if (currentlyRunningTask != null)
                Task.WaitAny(currentlyRunningTask);
            Task.WaitAny(threadExecuter);
        }

        /// <summary>
        /// Gets whether the Executor is shutdown or not
        /// </summary>
        public bool IsShutdown
        {
            get
            {
                return isShutdown;
            }
        }

        /// <summary>
        /// Disposes SequentialTaskExecuter
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool isDisposing)
        {
            if (!isDisposed)
            {
                StopAllExecutions();
                isShutdown = true;
                taskCancellationTokenSource.Dispose();
                executerCancellationTokenSource.Dispose();

                if (null != queue)
                {
                    queue.CompleteAdding();
                    queue.Dispose();
                    queue = null;
                }
                isDisposed = true;
            }
        }

        ~SequentialTaskExecuter()
        {
            Dispose(false);
        }

    }
}
