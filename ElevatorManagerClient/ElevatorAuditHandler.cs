﻿using Eurofins.ElevatorManager;
using Mediator.Net.Context;
using Mediator.Net.Contracts;
using System.Threading;
using System.Threading.Tasks;
using Unity;

namespace ElevatorManagerClient
{
    /// <summary>
    /// This class received all state changes and updated audit log
    /// </summary>
    class ElevatorAuditHandler : IEventHandler<ElevatorStateChangedEvent>
    {       
        public Task Handle(IReceiveContext<ElevatorStateChangedEvent> context, CancellationToken cancellationToken)
        {
            var state = context.Message;            

            return Task.Run(() =>
            {                
                var logData = string.Format("Id:{0}, Elevator:{1}, Floor:{2}, State:{3}, Direction:{4}, ThreadId:{5}", state.EventId, state.ElevatorStatus.ElevatorName, state.ElevatorStatus.CurrentFloor, state.ElevatorStatus.State, state.ElevatorStatus.Direction, state.ThreadId);
                Eurofins.Infrastructure.Environment.Instance.UnityContainer.Resolve<AuditLog>().LogData(logData);
            });

        }
    }
}
