﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace ElevatorManagerClient
{
    /// <summary>
    /// This class keep tracks all changes in the elevator state and also prints to console
    /// </summary>
    public class AuditLog
    {
        private readonly ReaderWriterLockSlim stateLock;
        private readonly StringBuilder logInformation;

        public AuditLog()
        {
            stateLock = new ReaderWriterLockSlim();
            logInformation = new StringBuilder();
        }

        public void LogData(string data)
        {
            stateLock.EnterWriteLock();
            try
            {
                logInformation.AppendLine(data);
                Console.WriteLine(data);
            }
            finally
            {
                stateLock.ExitWriteLock();
            }
        }

        public string GetLogData()
        {
            return logInformation.ToString();
        }
    }
}
