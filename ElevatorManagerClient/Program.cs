﻿using Eurofins.ElevatorManager;
using Mediator.Net.Context;
using Mediator.Net.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Unity;

namespace ElevatorManagerClient
{
    class Program 
    {
        private static ElevatorSynchronizer synchronizer;
        static void Main(string[] args)
        {
            Eurofins.Infrastructure.Environment.Instance.UnityContainer.RegisterInstance<AuditLog>(new AuditLog());
            var builder = new ElevatorBuilder();
            builder.SetElevatorDoorMovementDuration(5000) //5 sec to open/close door
                    .SetElevatorFloorMovementDuration(50) //5 sec to move to next floor
                    .SetFloorServiceRange(-2, 10) //min and max floor
                    .SetPassengerWaitTime(1000) //5 sec for people to move in/out of elevator
                    .AddElevator("E1")
                    .AddElevator("E2")
                    .AddElevator("E3");
            synchronizer = builder.Build();

            synchronizer.ServeFloor(7, ElevatorMovementDirection.Up);
            Thread.Sleep(4000); //wait for E1 lift to reach 7th floor
            synchronizer.ServeFloor(4, ElevatorMovementDirection.Down);
            Thread.Sleep(5000);//wait for E2 lift to reach 4th floor
            synchronizer.ServeFloor(3, ElevatorMovementDirection.Up); //this request is expected to be served by E2            
            Thread.Sleep(35000);
        }
        
    }


}
